package com.softnaya.energy1011.votes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private var btnStart: Button? = null
    private var btnFinish: Button? = null

    private var btn1: Button? = null
    private var btn2: Button? = null

    private var counter1: Int = 0;
    private var counter2: Int = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var btn1= findViewById<Button>(R.id.button1)
        var btn2= findViewById<Button>(R.id.button2)
        var btnStart= findViewById<Button>(R.id.btnStart)
        var btnFinish= findViewById<Button>(R.id.btnFinish)

        var textResult= findViewById<TextView>(R.id.textResult)

        btn1.setOnClickListener({
                Toast.makeText(this@MainActivity, "Gracias por votar", 0).show()
                counter1++;
        })

        btn2.setOnClickListener({
                Toast.makeText(this@MainActivity, "Gracias por votar",0).show()
                counter2++;
        })

        btnStart.setOnClickListener({
            textResult.setText("Votación iniciada.")
            counter1 = 0
            counter2 = 0
        })

        btnFinish.setOnClickListener({
            var winner = ""
            if (counter1 > counter2) winner = "Opción 1 " else winner = "Opición 2"
            if (counter1 == counter2) winner = "Empate!!"
            var total = counter1 + counter2
            textResult.setText("Ganador:"+winner+ "!! \nTotal de votantes:"+ total)

        })

    }
}
